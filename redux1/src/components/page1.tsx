import React from 'react'
import ContactForm from './contactForm'

export default function ContactPage() {
  const submit = (values: any) => {
    // print the form values to the console
    console.log(values)
  }
    return <ContactForm onSubmit={submit} />
}