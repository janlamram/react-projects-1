app.component("product-display", {
  props: {
    premium: {
      type: Boolean,
      required: true,
    },
  },
  template:
    /*html*/
    `<div class="product-display">
    <div class="product-container">
      <div class="product-image">
        <img v-bind:src="inStock ? image : outOfStockImg" />
      </div>
      <div class="product-info">
        <h1>{{ title }}</h1>
        <p v-show="inStock">In stock</p>
        <p v-if="onSale === true">On Sale</p>
        <p>Shipping: {{ shipping }}</p>
        <ul>
          <li v-for="detail in details">{{detail}}</li>
        </ul>
        <div
          v-for="(variant, index) in variants"
          class="color-circle"
          :style="{backgroundColor: variant.color}"
          :key="variant.id"
          @mouseover="updateVariant(index)"
        >
          {{variant.color}}
        </div>
        <button
          class="button"
          @click="addToCart"
          :disabled="!inStock"
          :class="{disabledButton: !inStock}"
        >
          Add
        </button>
        <button
          class="button"
          @click="removeFromCart"
        >
          Remove
        </button>
      </div>
    </div>
    <review-form @review-submitted="addReview"></review-form>
  </div>`,
  data() {
    return {
      product: "Socks",
      brand: "Vue Mastery",
      image: "./assets/images/socks_blue.jpg",
      outOfStockImg: "./assets/images/out.jpg",
      selectedVariant: 0,
      onSale: true,
      details: ["Toto", "Tamto", "Oko"],
      variants: [
        {
          id: 1,
          color: "green",
          image: "./assets/images/socks_green.jpg",
          quantity: 50,
        },
        {
          id: 2,
          color: "blue",
          image: "./assets/images/socks_blue.jpg",
          quantity: 0,
        },
      ],
      reviews: []
    };
  },
  methods: {
    addToCart() {
      this.$emit("add-to-cart", this.variants[this.selectedVariant].id);
    },
    removeFromCart() {
      this.$emit("remove-from-cart", this.variants[this.selectedVariant].id);
    },
    updateVariant(index) {
      this.selectedVariant = index;
    },
    addReview(review){
        this.reviews.push(review)
    }
  },
  computed: {
    title() {
      return this.brand + " " + this.product;
    },
    image() {
      return this.variants[this.selectedVariant].image;
    },
    inStock() {
      return this.variants[this.selectedVariant].quantity;
    },
    shipping() {
      if (this.premium) {
        return "Free";
      }
      return 2.99;
    },
  },
});
