// const w = require("./mod/writeFile.js");
const { readFile, writeFile } = require("fs").promises;

// const util = require("util");

// const readFilePromise = util.promisify(readFile)
// const writeFilePromise = util.promisify(writeFile)

// const http = require("http");

// const getText = (path) => {
//   return new Promise((resolve, reject) => {
//     readFile(path, "utf8", (err, data) => {
//       if (err) {
//         reject(err);
//         return;
//       } else {
//         resolve(data);
//       }
//     });
//   });
// };

// getText("./content/first.txt")
//   .then((result) => console.log("Result ", result))
//   .catch((err) => console.log("Err ", err));

const start = async () => {
  try {
    const first = await readFile("./content/first.txt", "utf-8");
    // const first = await getText("./content/first.txt");
    const second = await readFile("./content/second.txt", "utf-8");
    // const second = await getText("./content/second.txt");
    await writeFile("./content/three", `Dohromady : ${first} --- ${second}`, {
      flag: "a",
    });
    console.log(first, second);
  } catch (error) {
    console.log(error);
  }
};

start();

// const server = http.createServer((req, res) => {
//   if (req.url === "/") {
//     res.end('<h1>Welcome to myyyyyqqqq</h1> <a href="/about">About</a>');
//   }

//   if (req.url === "/about") {
//     res.end('<h1>This is - about</h1> <a href="/">Domu</a>');
//   }

//   // if (req.url === './*'){
//   // res.end(`<h1>Nicccc</h1>`);
//   // }
// });

// server.listen(5000, () => {
//   "Server is listening on port: 5000.";
// });
