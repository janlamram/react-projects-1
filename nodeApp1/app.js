const Emmiter = require('events')

const customEmmiter = new Emmiter()

customEmmiter.on('response', ({id, name}) => {
    // const {id, name} = props;
    console.log(`id: ${id} - Name: ${name}`)
})

customEmmiter.emit('response', {id: 20, name: 'John'})


const http = require('http')

const server = http.createServer();

server.on('request', (req, res) => {
    res.end('Wellllll')
})

server.listen(5000, () => {
  "Server is listening on port: 5000.";
});