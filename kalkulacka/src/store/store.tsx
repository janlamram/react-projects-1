import React from 'react';
import createStore from 'use-global-hook-ts';
import logic from '../logic/logic';
import { initialState, IAppState } from './store-types';

export const { useGlobal, store } = createStore(React, initialState, {
  debug: true
});

export const actions = {
  handleAction: (targetText:string) => {
    logic(targetText, store.state, actions);
  },
  setState: (props: IAppState) => {
    store.setState({...store.state, ...props})
  }
}

document.addEventListener("keydown", (event: KeyboardEvent) => {
  actions.handleAction(event.key);
});
