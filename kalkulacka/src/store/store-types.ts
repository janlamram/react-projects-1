export interface IActions {
  handleAction: (targetText: string) => void,
  setState: (props: any) => void
}

export interface IAppState {
  numbers: [number|string],
  operation: string|null,
  result: number,
  waitingForOperation: boolean,
  equalMark: boolean
}

export const initialState: IAppState = {
  numbers: [0],
  result: 0,
  operation: '+',
  waitingForOperation: false,
  equalMark: false
}