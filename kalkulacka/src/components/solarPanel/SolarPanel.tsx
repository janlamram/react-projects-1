import { ReactElement, useEffect, useState } from "react";

export default function SolarPanel(): ReactElement {
  let solarStyleInit = {
    backgroundColor: "yellow",
    border: "2px solid grey",
    width: "15px",
    height: "15px",
    margin: "2px",
  };

  const [solarStyle, setSolarStyle] = useState(solarStyleInit);

  useEffect(() => {
    let _interval = true;
    setInterval(function () {
      if (_interval) {
        setSolarStyle({
          ...solarStyle,
          backgroundColor: "red",
        });
        _interval = false;
      } else {
        setSolarStyle({
          ...solarStyle,
          backgroundColor: "yellow",
        });
        _interval = true;
      }
    }, 1000);
  }, []);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
      }}
    >
      <span
        style={{
          height: "2rem",
          display: "flex",
          alignItems: "center",
          fontSize: ".86em",
          marginTop: ".4rem",
          fontFamily: "Papyrus",
        }}
      >
        Bařtík 5000 © Solar System
      </span>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          marginLeft: "50px",
          alignItems: "center",
        }}
      >
        <div style={solarStyle}></div>
        <div style={solarStyle}></div>
        <div style={solarStyle}></div>
        <div style={solarStyle}></div>
      </div>
    </div>
  );
}
