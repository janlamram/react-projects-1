import { Button, Grid } from "@material-ui/core";
import { actions } from "../../store/store";

const CalcBody = () => {

  const handleClick = (event: React.MouseEvent<Record<string, any>>) => {
    actions.handleAction(event.currentTarget.outerText);
  };

  const styleActionBtn = {backgroundColor:'#D0D0D0', margin: '0.5em'}
  const styleBtn = {backgroundColor:'#DCDCDC', margin: '0.5em'}

  return (
    <div>
      <Grid>
        <Button style={styleActionBtn} onClick={handleClick}>CE</Button>
        <Button style={styleActionBtn} onClick={handleClick}>C</Button>
        <Button style={styleActionBtn} onClick={handleClick}>Del</Button>
        <Button style={styleActionBtn} onClick={handleClick}>/</Button>
      </Grid>
      <Grid>
        <Button style={styleBtn} onClick={handleClick}>7</Button>
        <Button style={styleBtn} onClick={handleClick}>8</Button>
        <Button style={styleBtn} onClick={handleClick}>9</Button>
        <Button style={styleActionBtn} onClick={handleClick}>*</Button>
      </Grid>
      <Grid>
        <Button style={styleBtn} onClick={handleClick}>4</Button>
        <Button style={styleBtn} onClick={handleClick}>5</Button>
        <Button style={styleBtn} onClick={handleClick}>6</Button>
        <Button style={styleActionBtn} onClick={handleClick}>-</Button>
      </Grid>
      <Grid>
        <Button style={styleBtn} onClick={handleClick}>1</Button>
        <Button style={styleBtn} onClick={handleClick}>2</Button>
        <Button style={styleBtn} onClick={handleClick}>3</Button>
        <Button style={styleActionBtn} onClick={handleClick}>+</Button>
      </Grid>
      <Grid>
        <Button style={styleBtn} onClick={handleClick}>+/-</Button>
        <Button style={styleBtn} onClick={handleClick}>0</Button>
        <Button style={styleBtn} onClick={handleClick}>.</Button>
        <Button style={styleActionBtn} onClick={handleClick}>=</Button>
      </Grid>
    </div>
  );
};

export default CalcBody;
