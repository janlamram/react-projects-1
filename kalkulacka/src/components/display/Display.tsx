import { ReactElement } from "react";
import { useGlobal } from "../../store/store";

const Display = (): ReactElement => {
  const [globalState] = useGlobal()

  return (
    <div style={{ width: "100%", height: "3em", backgroundColor: "#E0E0E0", borderRadius: '10px', overflow: 'auto'}}>
      <span style={{float: 'right', paddingRight: '1em',fontSize: '1.4em', marginTop:'0.2em'}}>{globalState.numbers.length ? globalState.numbers : globalState.result }</span>
    </div>
  );
};

export default Display;
