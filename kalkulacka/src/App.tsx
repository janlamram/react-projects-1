import CalcBody from "./components/calcBody/CalcBody";
import Display from "./components/display/Display";
import SolarPanel from "./components/solarPanel/SolarPanel";

function App() {
  return (
    <div
      className="App"
      style={{
        width: "20em",
        border: "1px solid grey",
        padding: "0.5em",
        borderRadius: "10px",
        backgroundColor: "#F5F5F5",
      }}
    >
      <Display />
      <SolarPanel />
      <CalcBody />
    </div>
  );
}

export default App;
