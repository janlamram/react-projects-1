import { IActions, IAppState } from "../store/store-types";

export default function logic(targetText: string, globalState:IAppState, actions: IActions): void {
  
  const CalculatorOperations: Record<string, any> = {
    '/': (prevValue: number, nextValue: number) => prevValue / nextValue,
    '*': (prevValue: number, nextValue: number) => prevValue * nextValue,
    '+': (prevValue: number, nextValue: number) => prevValue + nextValue,
    '-': (prevValue: number, nextValue: number) => prevValue - nextValue,
    '=': (prevValue: number, nextValue: number) => nextValue
  }
  const numbers = globalState.numbers;

    if ( !isNaN(+targetText) && globalState.equalMark === false){
      if (numbers.length === 1 && numbers[0] === 0){
        actions.setState({ numbers: [+targetText], waitingForOperation: false });
      }else{
        actions.setState({numbers: [...numbers, +targetText], waitingForOperation: false});
      }
    }else{

      if (targetText === 'DEL' || targetText === 'Backspace'){
        if(numbers.length > 1){
        numbers.pop();
        actions.setState({numbers: numbers});
        }
      };

      if (targetText === '.'){
        if (!numbers.includes('.') &&  globalState.waitingForOperation === false){
        actions.setState({ numbers: [...numbers, '.'] });
        }
      };

      if (targetText === '+/-'){
        if (+numbers.join('') !== 0){
          const converted = +numbers.join('') * -1;
          const digits = (""+converted).split("");
          actions.setState({ numbers: digits}) 
        }
      };

      if (targetText === 'C' || targetText === 'Delete'){
        actions.setState({numbers: [], result: 0, operation: '+',  waitingForOperation: false, equalMark: false});
      };
      
      if (targetText === 'CE' && globalState.equalMark === false && globalState.waitingForOperation === false){
        actions.setState({numbers: [0]});
      };

      if (targetText === '+' || targetText === '-' || targetText === '*' || targetText === '/'){
        if(globalState.waitingForOperation){
          actions.setState({operation: targetText, equalMark: false})
        }else{
          const operator = globalState.operation;
          const newValue: number = operator && CalculatorOperations[operator](globalState.result, +numbers.join(''))
          actions.setState({result: newValue, numbers: [], operation: targetText, waitingForOperation: true, equalMark: false});
        }
      }
      if ((targetText === '=' || targetText === 'Enter') && globalState.waitingForOperation === false && globalState.result !==0 && numbers !== [0]){
        const operator = globalState.operation;
        const newValue: number = operator && CalculatorOperations[operator](globalState.result, +numbers.join(''))
        actions.setState({result: newValue, numbers: [], waitingForOperation: true, equalMark: true})
      }
    };
}