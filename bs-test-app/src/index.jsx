import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { CombinationPage } from "./components/CombinationPage";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { StoreProvider } from "easy-peasy";
import { store } from "./store/store";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <StoreProvider store={store}>
    <React.StrictMode>
      <Router>
        <Routes>
          <Route path="/" element={<App />}></Route>
          <Route path="combinationpage" element={<CombinationPage />} />
        </Routes>
      </Router>
    </React.StrictMode>
  </StoreProvider>
);
