import { StoreProvider } from "easy-peasy";
import "./App.css";
import { MatrixPage } from "./components/MatrixPage";
import { store } from "./store/store";

function App() {
  return <MatrixPage />;
}

export default App;
