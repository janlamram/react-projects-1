import { ReactElement, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useGlobalStore } from "../store/store";

export const MatrixPage = (): ReactElement => {
  let navigate = useNavigate();
  const { actions } = useGlobalStore();

  const [state, setState] = useState<any>({
    numbers: [],
    combinations: { c1: [], c2: [], c3: [] },
    fewNums: false,
  });

  const getRandNum = () => {
    return state.numbers[Math.floor(Math.random() * state.numbers.length)];
  };

  const handleBoxClick = (event: any) => {
    const eTargetVal = event.target.value;
    const box: any = document.getElementById(eTargetVal);

    if (state.numbers.includes(Number(eTargetVal))) {
      box.style.backgroundColor = "aqua";
      const index = state.numbers.indexOf(Number(eTargetVal));
      if (index > -1) {
        state.numbers.splice(index, 1);
        const newNumbers = state.numbers;
        setState({
          ...state,
          numbers: newNumbers,
        });
      }
    } else {
      setState({
        ...state,
        numbers: [...state.numbers, Number(event.target.value)],
      });
      box.style.backgroundColor = "red";
    }
  };

  let i = 0;
  let c = 0;
  let comb: Array<number> = [];
  let c1: Array<number> = [];
  let c2: Array<number> = [];
  let c3: Array<number> = [];
  const getCombination = () => {
    const randNum = getRandNum();
    if (i < 6) {
      if (!comb.includes(randNum)) {
        comb.push(randNum);
        i++;
      }
      getCombination();
    } else {
      if (c === 0) {
        c1.push(...comb);
        i = 0;
        c++;
        comb = [];
        getCombination();
      } else if (c === 1) {
        c2.push(...comb);
        i = 0;
        c++;
        comb = [];
        getCombination();
      } else if (c === 2) {
        c3.push(...comb);
        c++;
        comb = [];
      }
    }
    setState({ ...state, combinations: { c1, c2, c3 }, fewNums: false });
    actions.updateCombinations({ c1, c2, c3 });
    return;
  };

  const handleCombinations = () => {
    if (state.numbers.length < 8) {
      setState({ ...state, fewNums: true });
    } else {
      setState({ ...state, fewNums: false });
      getCombination();
      navigate("/combinationpage");
    }
  };

  const boxes = () => {
    const boxes = [];
    for (let i = 1; i < 50; i++) {
      boxes.push(
        <input
          id={`${i}`}
          type="button"
          className="label"
          onClick={handleBoxClick}
          value={i}
          key={i}
        />
      );
    }
    return boxes;
  };

  return (
    <div className="App">
      <div className="matrix">{boxes()}</div>
      {state.fewNums && <p>Musíte zadat alespoň 8 čísel.</p>}
      <button type="button" onClick={handleCombinations}>
        Create combinations
      </button>
    </div>
  );
};
