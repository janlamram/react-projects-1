import { ReactElement, useEffect, useState } from "react";
import { useGlobalStore } from "../store/store";

export const CombinationPage = (): ReactElement => {
  const { gState } = useGlobalStore();

  const [url, setUrl] = useState("");

  useEffect(() => {
    const c1 = gState.combinations.c1;
    const c2 = gState.combinations.c2;
    const c3 = gState.combinations.c3;

    const _url = `https://www.sazka.cz/loterie/sportka/vsadit-si-online?wager={"b":[{"p":[${c1}]},{"p":[${c2}]},{"p":[${c3}]}]}`;
    setUrl(_url);
  }, [url]);

  return (
    <div>
      <h3>Your combinations</h3>
      <div>
        <div>
          Tip 1:
          {gState.combinations.c1.map((el: any) => (
            <span key={el} style={{ margin: ".5rem" }}>
              {el}
            </span>
          ))}
        </div>
        <div>
          Tip 2:
          {gState.combinations.c2.map((el: any) => (
            <span key={el} style={{ margin: ".5rem" }}>
              {el}
            </span>
          ))}
        </div>
        <div>
          Tip 3:
          {gState.combinations.c3.map((el: any) => (
            <span key={el} style={{ margin: ".5rem" }}>
              {el}
            </span>
          ))}
        </div>
      </div>
      <a href={url} target="_blank">
        <input type="button" value="Send" />
      </a>
    </div>
  );
};
