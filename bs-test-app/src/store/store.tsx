import {
  action,
  createStore,
  useStoreActions,
  useStoreState,
} from "easy-peasy";

interface StateProps {
  combinations: {
    c1: Array<number>;
    c2: Array<number>;
    c3: Array<number>;
  };
}

export const store = createStore({
  //---STATE---
  combinations: { c1: [], c2: [], c3: [] },

  //---ACTIONS---
  setCombinations: action((state: any, payload) => {
    state.combinations = payload;
  }),
});

export const useGlobalStore = () => {
  const state = useStoreState((state: StateProps) => state);

  const updateCombinations = useStoreActions(
    (actions: any) => actions.setCombinations
  );

  return {
    gState: state,
    actions: { updateCombinations },
  };
};
