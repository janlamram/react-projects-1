export default function genCounts(matrixLength: number): Array<Array<number>> {
  const counts: Array<Array<number>> = [];

  for (let row = 0; row < matrixLength; row++) {
    const currentRow: Array<number> = [];
    for (let col = 0; col < matrixLength; col++) {
      currentRow.push(0);
    }
    counts.push(currentRow);
  }

  return counts;
}
