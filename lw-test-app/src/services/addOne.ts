import { MatrixStateProps } from "../models";

export default function addOne(
  matrixState: MatrixStateProps
): Array<Array<number>> {
  const counts = matrixState.counts;

  if (
    matrixState.selectedCoords.row !== undefined &&
    matrixState.selectedCoords.col !== undefined
  ) {
    for (let i = 0; i < counts.length; i++) {
      counts[matrixState.selectedCoords.row][i] += 1;

      if (i !== matrixState.selectedCoords.row) {
        counts[i][matrixState.selectedCoords.col] += 1;
      }
    }
  }

  return counts;
}
