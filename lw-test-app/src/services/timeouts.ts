export default function timeouts(){
    setTimeout(() => {
        const elems = document.getElementsByTagName("input");
        for (let i = 0; i <= elems.length; i++) {
          elems[i]?.classList.contains("active") &&
            elems[i].classList.remove("active");
        }
      }, 2000);
  
      setTimeout(() => {
        const elems = document.getElementsByTagName("input");
        for (let i = 0; i <= elems.length; i++) {
          elems[i]?.classList.contains("isFibo") &&
            elems[i].classList.remove("isFibo");
        }
      }, 3000);
}