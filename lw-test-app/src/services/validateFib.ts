import { isFibonacci } from "../utils";

export default function validateFib(
  counts: Array<Array<number>>,
  fibLength: number
): Array<Array<number>> {
  let countFib: number = 0;
  let fibCoords: Array<Array<number>> = [];

  counts.forEach((row: Array<number>, rowIdx: number) => {
    for (let colIdx = 2; colIdx < row.length; colIdx++) {
      if (
        counts[rowIdx][colIdx] !== 0 &&
        counts[rowIdx][colIdx - 1] !== 0 &&
        counts[rowIdx][colIdx - 2] !== 0 &&
        isFibonacci(counts[rowIdx][colIdx]) &&
        counts[rowIdx][colIdx] ===
          counts[rowIdx][colIdx - 1] + counts[rowIdx][colIdx - 2]
      ) {
        if (countFib === 0) {
          countFib = 2;
          fibCoords.push([rowIdx, colIdx - 1], [rowIdx, colIdx - 2]);
        }
        countFib++;
        fibCoords.push([rowIdx, colIdx]);
      } else {
        if (countFib >= fibLength) {
          fibCoords.forEach((coords: Array<number>) => {
            counts[coords[0]][coords[1]] = 0;
            const el = document.getElementById(`${coords[0]}-${coords[1]}`);
            el?.classList.add("isFibo");
          });
        }
        countFib = 0;
        fibCoords = [];
      }
    }
  });

  counts.forEach((row: Array<number>, colIdx: number) => {
    for (let rowIdx = 2; rowIdx < row.length; rowIdx++) {
      if (
        counts[rowIdx][colIdx] !== 0 &&
        counts[rowIdx - 1][colIdx] !== 0 &&
        counts[rowIdx - 2][colIdx] !== 0 &&
        isFibonacci(counts[rowIdx][colIdx]) &&
        counts[rowIdx][colIdx] ===
          counts[rowIdx - 1][colIdx] + counts[rowIdx - 2][colIdx]
      ) {
        if (countFib === 0) {
          countFib = 2;
          fibCoords.push([rowIdx - 1, colIdx], [rowIdx - 2, colIdx]);
        }
        countFib++;
        fibCoords.push([rowIdx, colIdx]);
      } else {
        if (countFib >= fibLength) {
          fibCoords.forEach((coords: Array<number>) => {
            counts[coords[0]][coords[1]] = 0;
            const el = document.getElementById(`${coords[0]}-${coords[1]}`);
            el?.classList.add("isFibo");
          });
        }
        countFib = 0;
        fibCoords = [];
      }
    }
  });

  return counts;
}
