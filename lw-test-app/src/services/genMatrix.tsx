import { ReactElement } from "react";
import CustomButton from "../components/CustomButton";
import { MatrixStateProps } from "../models";

export default function genMatrix(
  matrixState: MatrixStateProps,
  setMatrixState: Function,
  matrixLength: number
): Array<Array<ReactElement>> {
  const matrix: Array<Array<ReactElement>> = [];

  const getIsActive = (row: number, col: number): void => {
    if (
      row === matrixState.selectedCoords.row ||
      col === matrixState.selectedCoords.col
    ) {
      const el = document.getElementById(`${row}-${col}`);
      el?.classList.add("active");
    } else {
      const el = document.getElementById(`${row}-${col}`);
      el?.classList.remove("active");
    }
  };

  for (let row = 0; row < matrixLength; row++) {
    const currentRow: Array<ReactElement> = [];
    for (let col = 0; col < matrixLength; col++) {
      getIsActive(row, col);

      currentRow.push(
        <CustomButton
          {...{
            value: matrixState?.counts.length
              ? matrixState?.counts[row][col]
              : 0,
            matrixState,
            setMatrixState,
            row,
            col,
            id: `${row}-${col}`,
          }}
        />
      );
    }
    matrix.push(currentRow);
  }

  return matrix;
}
