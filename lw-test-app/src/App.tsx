import { useEffect, useState } from "react";
import "./App.css";
import { MatrixStateProps } from "./models";
import addOne from "./services/addOne";
import genCounts from "./services/genCounts";
import timeouts from "./services/timeouts";
import validateFib from "./services/validateFib";
import Matrix from "./components/Matrix";
import { MatrixToolbar } from "./components/MatrixToolbar";

function App() {
  const [matrixState, setMatrixState] = useState<MatrixStateProps>({
    selectedCoords: { row: undefined, col: undefined },
    counts: [],
    matrixLength: 15,
    fibLength: 5,
  });

  useEffect(() => {
    setMatrixState({
      ...matrixState,
      counts: genCounts(matrixState.matrixLength),
    });
  }, [matrixState.matrixLength]);

  useEffect(() => {
    if (matrixState.counts.length) {
      let counts = addOne(matrixState);
      counts = validateFib(counts, matrixState.fibLength);
      setMatrixState({ ...matrixState, counts });
      timeouts();
    }
  }, [matrixState.selectedCoords, matrixState.fibLength]);

  return (
    <div className="App">
      <MatrixToolbar {...{ matrixState, setMatrixState }} />
      <Matrix {...{ matrixState, setMatrixState }} />
    </div>
  );
}

export default App;
