function isPerfectSquare(x: number) {
  let s = parseInt(String(Math.sqrt(x)));
  return s * s == x;
}

export function isFibonacci(n: number) {
  return isPerfectSquare(5 * n * n + 4) || isPerfectSquare(5 * n * n - 4);
}
