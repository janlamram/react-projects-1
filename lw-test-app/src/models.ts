export interface MatrixStateProps {
  selectedCoords: { row: undefined | number; col: undefined | number };
  counts: Array<Array<number>>;
  matrixLength: number;
  fibLength: number;
}
