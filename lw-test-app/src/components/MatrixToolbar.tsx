import { ReactElement } from "react";

export const MatrixToolbar = (props: any): ReactElement => {
  const { matrixState, setMatrixState } = props;
  return (
    <div className="matrixToolbar">
      <div className="matrixToolbarItem">
        <label style={{ marginRight: "1em" }}>
          Matrix length (between 10 and 30):
        </label>

        <input
          type="number"
          id="quantity-matrixLength"
          min="10"
          max="30"
          className="matrixToolbarInput"
          value={matrixState.matrixLength}
          onChange={(event) =>
            setMatrixState({
              ...matrixState,
              selectedCoords: { row: undefined, col: undefined },
              counts: [],
              matrixLength: event.target.value,
            })
          }
        ></input>
      </div>
      <div className="matrixToolbarItem">
        <label style={{ marginRight: "1em" }}>
          Fibonacci sequence length (between 3 and 8):
        </label>
        <input
          type="number"
          id="quantity-fibLength"
          className="matrixToolbarInput"
          min="3"
          max="8"
          value={matrixState.fibLength}
          onChange={(event) =>
            setMatrixState({
              ...matrixState,
              selectedCoords: { row: undefined, col: undefined },
              fibLength: event.target.value,
            })
          }
        ></input>
      </div>
    </div>
  );
};
