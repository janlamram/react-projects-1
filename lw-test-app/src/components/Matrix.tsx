import { ReactElement } from "react";
import { MatrixStateProps } from "../models";
import genMatrix from "../services/genMatrix";

export default function Matrix(props: {
  matrixState: MatrixStateProps;
  setMatrixState: Function;
}): ReactElement {
  const { matrixState, setMatrixState } = props;
  return (
    <div className="matrix">
      {genMatrix(matrixState, setMatrixState, matrixState.matrixLength).map(
        (row: Array<ReactElement>, rowId: number) => (
          <div style={{ display: "flex" }} key={rowId}>
            {row.map((node: ReactElement, nodeId: number) => (
              <span style={{ display: "flex" }} key={nodeId}>
                {node}
              </span>
            ))}
          </div>
        )
      )}
    </div>
  );
}
