import { ReactElement } from "react";

export default function CustomButton(props: any): ReactElement {
  const { value, matrixState, setMatrixState, row, col, id } = props;

  const handleClick = () => {
    setMatrixState({ ...matrixState, selectedCoords: { row, col } });
  };

  return (
    <input
      id={id}
      key={id}
      className="button"
      type="button"
      value={value === 0 ? "" : value}
      onClick={handleClick}
    />
  );
}
