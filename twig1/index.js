const app = require('express')();
const port = process.env.NODE_PORT || 3001;
const {TwingEnvironment, TwingLoaderFilesystem} = require('twing');
let loader = new TwingLoaderFilesystem('./templates');
let twing = new TwingEnvironment(loader);
      
app.get('/', function (req, res) {
  twing.render('index.twig', {'name': 'World', 'neco': {'id': 1, 'name': 'Karel'}}).then((output) => {
      res.end(output);
  });
});
      
app.get('/name/:name', function (req, res) {      
  twing.render('index.twig', req.params).then((output) => {
    res.end(output);
  });
});
      
app.listen(port, () => {
  console.log('Node.js Express server listening on port '+port);
});