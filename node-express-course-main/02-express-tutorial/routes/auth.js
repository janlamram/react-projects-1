const express = require("express");
const router = express.Router();

// const { people } = require("../data");

router.post("/", (req, res) => {
  console.log(req.body);
  const { name } = req.body;

  if (name) {
    return res.status(200).send(`Name ${name}`);
  }

  res.status(401).send("Provide credentials.");
});

module.exports = router;
