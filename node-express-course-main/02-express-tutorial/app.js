const express = require("express");
const path = require("path");
const app = express();



// const { products, people } = require("./data");
// const authorize = require("./authorize.js");
// const { read } = require("fs");

// app.use(express.static('./public'))

// app.get('/', (req, res) => {
//     res.sendFile(path.resolve(__dirname, './navbar-app/index.html'))})

const logger = (req, res, next) => {
  const method = req.method;
  const url = req.url;
  const time = new Date().getFullYear();

  console.log(method, url, time);

  //musí být posláno do následující fce, jinak termninating
  next();
};

// app.use([logger, authorize]);

// app.use('/about', logger);

app.use(express.static("./methods-public"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const people = require('./routes/people')
const auth = require('./routes/auth')
app.use('/api/people', people);
app.use('/login', auth);

// app.post("/login", (req, res) => {
//   console.log(req.body);
//   const { name } = req.body;

//   if (name) {
//     return res.status(200).send(`Name ${name}`);
//   }

//   res.status(401).send("Provide credentials.");
// });

// app.get("/api/people", (req, res) => {
//   res.status(200).json({ success: true, data: people });
// });

// app.post("/api/people", (req, res) => {
//   const { name } = req.body;

//   if (!name) {
//     res.status(401).json({ success: false, msg: "Provide name value " });
//   }
//   res.status(201).send({ success: true, person: name });
// });

// app.post("/api/postman/people", (req, res) => {
//   const { name } = req.body;

//   if (!name) {
//     res.status(401).json({ success: false, msg: "Provide name value " });
//   }
//   res.status(201).send({ success: true, data: [...people, { name: name }] });
// });

// app.put("/api/people/:id", (req, res) => {
//   const { id } = req.params;
//   const { name } = req.body;

//   const person = people.find((person) => person.id === Number(id));

//   if (!person) {
//     res.status(404).json({ success: false, msg: `No person with ig ${id}` });
//   }

//   const newPeople = people.map((person) => {
//     if (person.id === Number(id)) {
//       person.name = name;
//     }
//     return person;
//   });

//   res.status(200).json({ success: true, data: newPeople });
// });

// app.delete("/api/people/:id", (req, res) => {
//   const { id } = req.params;
//   const { name } = req.body;

//   const person = people.find((person) => person.id === Number(id));

//   if (!person) {
//     res.status(404).json({ success: false, msg: `No person with ig ${id}` });
//   }

//   const newPeople = people.filter((person) => (person.id) !== Number(id));


//   res.status(200).json({ success: true, data: newPeople });
// });


//---------------------

// app.get("/", (req, res) => {
//   res.send('<h1>Home</h1><a href="/api/products">Products</a>');
// });

// app.get("/about", [logger, authorize], (req, res) => {
//   res.send('<h1>About</h1><a href="/api/products">Products</a>');
// });

// app.get("/", logger, (req, res) => {
//   res.send('<h1>Home</h1><a href="/api/products">Products</a>');
// });

// app.get("/about", logger, (req, res) => {
//   res.send('<h1>About</h1><a href="/api/products">Products</a>');
// });

//----------------------------------

// app.get("/", (req, res) => {
//   res.send('<h1>Home</h1><a href="/api/products">Products</a>');
// });

// app.get("/api/products", (req, res) => {
//   const newProducts = products.map((product) => {
//     const { id, name, image } = product;
//     return { id, name, image };
//   });
//   res.json(newProducts);
// });

// app.get("/api/products/:productID", (req, res) => {
// //   console.log("Reqqq ", req);
//   const singleProducts = products.find(
//     (product) => product.id === Number(req.params.productID)
//   );

//   if (!singleProducts) {
//     return res.status(404).send("Nothing here!");
//   }
//   return res.json(singleProducts);
// });

// app.get("/api/v1/query", (req, res) => {
//     console.log('query ', req.query);

//     const {search, limit} = req.query;

//     let sortedProducts = [...products];

//     if (search) {
//         sortedProducts = sortedProducts.filter((product) => {
//             return product.name.startsWith(search);
//         })
//     }

//     if (limit) {
//         sortedProducts = sortedProducts.slice(0, Number(limit));
//     }

//     if (sortedProducts.length < 1) {
//         // res.status(200).send('No such product')
//         return res.status(200).json({success: true, data : []})
//     }

//     res.status(200).json(sortedProducts);
// })

app.all("*", (req, res) => {
  res.status(404).send("Nicccc");
});
app.listen(5000, () => {
  console.log("Is listening");
});

// const http = require("http");
// const {readFileSync} = require('fs')

// const homePage = readFileSync('./navbar-app/index.html')

// const server = http.createServer((req, res) => {
//   console.log(req.url);

//   if (req.url === "/") {
//     res.writeHead(200, { "content-type": "text/html" });
//     res.write(homePage);
//     res.end();
//   } else if (req.url === "/about") {
//     res.writeHead(200, { "content-type": "text/html" });
//     res.write("<h1>About Page</h1>");
//     res.end();
//   } else {
//     res.writeHead(404, { "content-type": "text/html" });
//     res.write("<h1>Page not found</h1>");
//     res.end();
//   }
// });

// server.listen(5000);
