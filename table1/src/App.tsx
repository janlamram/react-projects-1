import "./App.css";
import CssBaseline from "@material-ui/core/CssBaseline";
import { NavLink, Outlet } from "react-router-dom";

function App() {
  const style = {textDecoration: 'none', padding: '1rem'};

  return (
    <div className="App" style={{margin: '1rem'}}>
      <CssBaseline />
      <nav style={{margin: '1rem'}}>
        <NavLink style={{...style,fontWeight: 'bold'}} to="/">
          Home
        </NavLink>{" "}
        |{" "}
        <NavLink style={style} to="/table1">
          Table 1
        </NavLink>{" "}
        |{" "}
        <NavLink style={style} to="/table2">
          Table 2
        </NavLink>{" "}
        | <NavLink style={style} to="/table3">Table 3</NavLink> |{" "}
        <NavLink style={style} to="/table4">Table 4</NavLink>
      </nav>
      <Outlet />
    </div>
  );
}

export default App;
