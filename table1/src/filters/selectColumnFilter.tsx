import { ReactElement, useMemo } from "react";
import { Box, FormControl, NativeSelect } from "@material-ui/core";

// a unique option from a list
export default function SelectColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id },
}: Record<string, any>): ReactElement {
  const options = useMemo(() => {
    const options: Record<string, any> = new Set();
    preFilteredRows.forEach((row: Record<string, any>) => {
      options.add(row.values[id]);
    });
    return [...options.values()];
  }, [id, preFilteredRows]);

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <NativeSelect
          style={{ width: "8rem", height: "1.3rem" }}
          defaultValue={""}
          value={filterValue}
          inputProps={{ "aria-label": "Without label" }}
          onChange={(e) => {
            setFilter(e.target.value || undefined);
          }}
        >
          <option value="">All</option>
          {options.map((option, i) => (
            <option key={i} value={option}>
              {option}
            </option>
          ))}
        </NativeSelect>
      </FormControl>
    </Box>
  );
}
