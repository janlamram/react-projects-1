import { ReactElement, useMemo } from "react";
// import Input from "@material-ui/core/Input";

export default function DateRangeColumnFilter({
  column: { filterValue = [], preFilteredRows, setFilter, id },
}: any): ReactElement {
  const _date = preFilteredRows[0].values[id].split(" ");
  const day = _date[0].slice(0, -1);
  const month = _date[1].slice(0, -1);
  const year = _date[2];
  const _time = _date[3].split(":");
  const _newDate = useMemo(
    () => new Date(year, month - 1, day, _time[0], _time[1], _time[2]),
    [day, month, year, _time]
  );

  console.log('Filterrr ', filterValue)

  const [min, max] = useMemo(() => {
    let min = preFilteredRows.length ? _newDate : new Date(0);
    let max = preFilteredRows.length ? _newDate : new Date(0);

    preFilteredRows.forEach((row: any) => {
      const _date = row.values[id].split(" ");
      const day = _date[0].slice(0, -1);
      const month = _date[1].slice(0, -1);
      const year = _date[2];
      const _time = _date[3].split(":");
      const rowDate = new Date(
        year,
        month - 1,
        day,
        _time[0],
        _time[1],
        _time[2]
      );

      min = rowDate <= min ? rowDate : min;
      max = rowDate >= max ? rowDate : max;
    });

    return [min, max];
  }, [id, preFilteredRows, _newDate]);

  return (
    <div>
      <input
        min={min.toISOString().slice(0, 10)}
        max={max.toISOString().slice(0, 10)}
        onChange={(e) => {
          const val = e.target.value;
          console.log('Valll ', val)
          setFilter((old = []) => { 
            console.log('Oooo ', [val ? val : undefined, old[1]])
            return [val ? val : undefined, old[1]]});
        }}
        type="date"
        value={filterValue[0] || ""}
      />
      {" to "}
      <input
        min={min.toISOString().slice(0, 10)}
        max={max.toISOString().slice(0, 10)}
        onChange={(e) => {
          const val = e.target.value;
          setFilter((old = []) => [
            old[0],
            // val ? val : undefined,
            val ? val.concat("T23:59:59.999Z") : undefined,
          ]);
        }}
        type="date"
        value={filterValue[1]?.slice(0, 10) || ""}
      />
    </div>
  );
}
