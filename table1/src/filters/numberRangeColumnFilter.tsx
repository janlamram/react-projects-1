import { ReactElement, useMemo } from "react";
import Input from "@material-ui/core/Input";

export default function NumberRangeColumnFilter({
  column: { filterValue = [], preFilteredRows, setFilter, id },
}: Record<string, any>): ReactElement {
  const [min, max] = useMemo(() => {
    let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    preFilteredRows.forEach((row: Record<string, any>) => {
      min = Math.min(row.values[id], min);
      max = Math.max(row.values[id], max);
    });
    return [min, max];
  }, [id, preFilteredRows]);

  return (
    <div>
      <Input
        value={filterValue[0] || ""}
        type="number"
        onChange={(e) => {
          const val = e.target.value;
          setFilter((old = []) => [
            val ? parseInt(val, 10) : undefined,
            old[1],
          ]);
        }}
        placeholder={`Min (${min})`}
        style={{
          width: "75px",
          marginRight: "0.5rem",
          height: "1.4rem",
        }}
      />
      to
      <Input
        value={filterValue[1] || ""}
        type="number"
        onChange={(e) => {
          const val = e.target.value;
          setFilter((old = []) => [
            old[0],
            val ? parseInt(val, 10) : undefined,
          ]);
        }}
        placeholder={`Max (${max})`}
        style={{
          width: "79px",
          marginLeft: "0.5rem",
          height: "1.4rem",
        }}
      />
    </div>
  );
}
