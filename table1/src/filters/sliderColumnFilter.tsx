import { ReactElement, useMemo } from "react";
import Button from "@material-ui/core/Button";

export default function SliderColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id },
}: Record<string, any>): ReactElement {
  const [min, max] = useMemo(() => {
    let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    preFilteredRows.forEach((row: any) => {
      min = Math.min(row.values[id], min);
      max = Math.max(row.values[id], max);
    });
    return [min, max];
  }, [id, preFilteredRows]);

  return (
    <div>
      <input
        style={{ maxWidth: "4rem", marginRight: ".5rem" }}
        type="range"
        min={min}
        max={max}
        value={filterValue || min}
        onChange={(e) => {
          setFilter(parseInt(e.target.value, 10));
        }}
      />
      <Button
        size="small"
        variant="outlined"
        onClick={() => setFilter(undefined)}
      >
        Off
      </Button>
    </div>
  );
}
