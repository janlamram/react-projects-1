export const customSorting = (a: any, b: any): any => {
  const date1 = new Date(a.values.created_at_Raw).getTime();
  const date2 = new Date(b.values.created_at_Raw).getTime();

  if (date1 < date2) {
    return -1;
  }
  if (date1 > date2) {
    return 1;
  }
  return 0;
};
