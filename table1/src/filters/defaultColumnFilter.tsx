import { ReactElement } from "react";
import Input from "@material-ui/core/Input";

// Define a default UI for filtering
export default function DefaultColumnFilter({
  column: { filterValue, preFilteredRows, setFilter },
}: Record<string, any>): ReactElement {
  const count = preFilteredRows.length;

  console.log('Prefiltered rows ', filterValue)

  return (
    <Input
      value={filterValue || ""}
      onChange={(e) => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      placeholder={`Search ${count} records...`}
      style={{height: '1.4rem'}}
    />
  );
}
