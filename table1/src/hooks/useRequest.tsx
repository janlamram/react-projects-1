import { useEffect, useState } from "react";

function getRandomInt(max: number): number {
  return Math.floor(Math.random() * max);
}

function getCustomDate(dateCode: string): any {
  // const date = dateCode.split("T")[0].split("-");
  const time = dateCode.split("T")[1].slice(0, -1).split(":");

  const day = getRandomInt(27) + 1;
  const month = getRandomInt(11) + 1;
  const hours = getRandomInt(23) + 1;
  const minutes = getRandomInt(59) + 1;

  const d = new Date(2021, month, day, hours, minutes, Number(time[2]));

  return d;
}

export default function useRequest(request: () => Promise<any>): any {
  const [state, setState] = useState({ data: [], isLoading: true });
  useEffect(() => {
    setTimeout(
      () =>
        request()
          .then((data) => {
            const _data: any = data.map((el: any) => {

              const date = getCustomDate(el.created_at);
              return {
                ...el,
                ...{
                  name: el.repo.name,
                  created_at_Raw: String(date),
                  created_at: date.toLocaleString(),
                  Quantity: getRandomInt(100),
                  Used: getRandomInt(10000),
                },
              };
            });

            setState({ data: _data.slice(0, 4000), isLoading: false });
          })
          .catch(() => console.error("No data")),
      3
    );
  }, [request]);

  return state;
}
