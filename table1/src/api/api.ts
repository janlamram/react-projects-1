const fetchData = async (url: string):  Promise<Record<string, number | boolean| string>> => {
  return await fetch(url).then((resp) => resp.json());
};

export const getDataFile1 = () => fetchData("json/file1.json");
export const getDataFile2 = () => fetchData("json/file2.json");
