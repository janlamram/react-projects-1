import * as React from "react";
import { Box, CircularProgress } from "@material-ui/core";

export default function Spinner() {
  return (
    <Box
      sx={{ display: "flex", position: "absolute", top: "50%", left: "50%" }}
    >
      <CircularProgress />
    </Box>
  );
}
