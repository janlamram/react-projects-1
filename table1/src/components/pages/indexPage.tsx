import { ReactElement } from "react";

export default function IndexPage(): ReactElement {

  const style = {display: 'flex', justifyContent:'flex-start'};

  return (
    <div style={{ display: 'flex', justifyContent: 'center'}}>
      <div style={{ width: "20rem", display: "flex", flexDirection: "column", justifyContent: 'flex-start' }}>
        <h1 style={style}>Select table</h1>
        <h3 style={style}>Table 1 containes sorting and filters.</h3>
        <h3 style={style}>Table 2 is the same as Table 1 <br />but with different columns.</h3>
        <h3 style={style}>Table 3 containes only sorting.</h3>
        <h3 style={style}>Table 4 is simple table.</h3>
      </div>
    </div>
  );
}
