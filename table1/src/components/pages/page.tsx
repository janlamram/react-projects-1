import { ReactElement } from "react";
import { useLocation } from "react-router-dom";
import { getDataFile1 } from "../../api/api";
import useRequest from "../../hooks/useRequest";
import Spinner from "../spinner";
import { columnsTable1 } from "../table/columnsTable1";
import { columnsTable2 } from "../table/columnsTable2";
import { columnsTable3 } from "../table/columnsTable3";
import { columnsTable4 } from "../table/columnsTable4";
import Table from "../table/table";

const pathConversion: Record<string, any> = {
  table1: columnsTable1,
  table2: columnsTable2,
  table3: columnsTable3,
  table4: columnsTable4,
};

export default function Page(): ReactElement {
  let { data, isLoading } = useRequest(getDataFile1);

  let { pathname } = useLocation();
  pathname = pathname.substring(1);
  const columnsTable = pathConversion[pathname];


  return isLoading ? (
    <Spinner />
  ) : (
    data.length ?
    <Table columns={columnsTable} data={data} isLoading={isLoading} /> : <></>
  );
}
