import { ReactElement } from "react";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";

type CustomPaginationProps = {
  canPreviousPage: boolean;
  canNextPage: boolean;
  pageOptions: Record<string, any>;
  pageCount: number;
  gotoPage: (pageCount: number) => void;
  nextPage: () => void;
  previousPage: () => void;
  setPageSize: (pageSize: number) => void;
  pageIndex: number;
  pageSize: number;
};

export default function CustomPagination(
  props: CustomPaginationProps
): ReactElement {
  const {
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    pageIndex,
    pageSize,
  } = props;

  return (
    <div className="pagination">
      <Button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
        {"<<"}
      </Button>{" "}
      <Button onClick={() => previousPage()} disabled={!canPreviousPage}>
        {"<"}
      </Button>{" "}
      <Button onClick={() => nextPage()} disabled={!canNextPage}>
        {">"}
      </Button>{" "}
      <Button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
        {">>"}
      </Button>{" "}
      <span>
        Page{" "}
        <strong>
          {pageIndex + 1} of {pageOptions?.length}
        </strong>{" "}
      </span>
      <span>
        | Go to page:{" "}
        <Input
          type="number"
          defaultValue={pageIndex + 1}
          onChange={(e) => {
            const page = e.target.value ? Number(e.target.value) - 1 : 0;
            gotoPage(page);
          }}
          style={{ width: "100px", margin: ".3rem" }}
        />
      </span>{" "}
      <Select
        value={pageSize}
        onChange={(e) => {
          setPageSize(Number(e.target.value));
        }}
      >
        {[10, 20, 30, 40, 50].map((pageSize) => (
          <option key={pageSize} value={pageSize}>
            Show {pageSize}
          </option>
        ))}
      </Select>
    </div>
  );
}
