import { ReactElement, useMemo } from "react";
import {
  useGlobalFilter,
  useFilters,
  useSortBy,
  useTable,
  usePagination,
} from "react-table";
import GlobalFilter from "../../filters/globalFilter";
import CustomPagination from "../customPagination";

import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

type useTableProps = {
  data: Record<string, any>;
  defaultColumn: Record<string, any>;
  columns: {
    getSortByToggleProps?: () => void;
    isSorted?: boolean;
    isSortedDesc?: boolean;
  };
  filterTypes?: Record<string, any>;
  useSortBy?: () => void;
};

type TableProps = {
  data: any;
  columns: any;
  isLoading: boolean;
};

export default function Table(props: TableProps): ReactElement {
  const defaultColumn: Record<string, any> = useMemo(
    () => ({
      Filter: "",
      // Filter: DefaultColumnFilter,
    }),
    []
  );

  console.log("Props ", props);

  const { data, isLoading, columns } = props;

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize, globalFilter },
    prepareRow,
    visibleColumns,
    preGlobalFilteredRows,
    setGlobalFilter,
  }: any = useTable<useTableProps>(
    {
      data,
      columns,
      defaultColumn,
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
    usePagination
  );

  return (
    <>
      {/* <pre>
        <code>
          {JSON.stringify(
            {
              pageIndex,
              pageSize,
              pageCount,
              canNextPage,
              canPreviousPage,
            },
            null,
            2
          )}
        </code>
      </pre> */}
      <MaUTable {...getTableProps()}>
        <TableHead>
          {headerGroups.map((headerGroup: any) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: any) => (
                <TableCell
                  {...column.getHeaderProps()}
                  style={{
                    background: "aliceblue",
                    color: "black",
                    fontWeight: "bold",
                    fontFamily: "Calibri",
                    verticalAlign: "top",
                  }}
                >
                  <div>
                    {column.render("Header")}
                    <span
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                    >
                      {column._isSorted
                        ? column.isSorted
                          ? column.isSortedDesc
                            ? " 🔽"
                            : " 🔼"
                          : " ⏹️"
                        : ""}
                    </span>
                  </div>
                  <div>{column.canFilter ? column.render("Filter") : null}</div>
                </TableCell>
              ))}
            </TableRow>
          ))}
          {columns[0].isGlobalFilter && (
            <TableRow>
              <TableCell
                colSpan={visibleColumns.length}
                style={{
                  textAlign: "left",
                }}
              >
                <GlobalFilter
                  preGlobalFilteredRows={preGlobalFilteredRows}
                  globalFilter={globalFilter}
                  setGlobalFilter={setGlobalFilter}
                />
              </TableCell>
            </TableRow>
          )}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.map((row: any, i: number) => {
            prepareRow(row);
            return (
              <TableRow {...row.getRowProps()}>
                {row.cells.map((cell: any) => {
                  return (
                    <TableCell
                      {...cell.getCellProps()}
                      style={{
                        fontFamily: "Calibri",
                        maxWidth: "8rem",
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                      }}
                    >
                      {cell.render("Cell")}
                    </TableCell>
                  );
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </MaUTable>
      <CustomPagination
        {...{
          canPreviousPage,
          canNextPage,
          pageOptions,
          pageCount,
          gotoPage,
          nextPage,
          previousPage,
          setPageSize,
          pageIndex,
          pageSize,
        }}
      />
    </>
  );
}
