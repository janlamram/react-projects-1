import SliderColumnFilter from "../../filters/sliderColumnFilter";
import NumberRangeColumnFilter from "../../filters/numberRangeColumnFilter";
import DefaultColumnFilter from "../../filters/defaultColumnFilter";
import SelectColumnFilter from "../../filters/selectColumnFilter";
import DateRangeColumnFilter from "../../filters/dateRangeColumnFilter";
import { customSorting } from "../../filters/customSorting";
// import DateRangeColumnFilter from "../../filters/dateRangeColumnFilter";

function filterGreaterThan(rows: any, id: any, filterValue: any) {
  return rows.filter((row: any) => {
    const rowValue = row.values[id];
    return rowValue >= filterValue;
  });
}

export const columnsTable1 = [
  {
    Header: "Table 1 - Filtering and sorting",
    isGlobalFilter: true,
    columns: [
      {
        Header: "ID",
        accessor: "id",
        _isSorted: true,
      },
      {
        Header: "Repo name",
        accessor: "name",
        Filter: DefaultColumnFilter,
        _isSorted: true,
      },
      {
        Header: "Type",
        accessor: "type",
        Filter: SelectColumnFilter,
        filter: "includes",
        _isSorted: true,
      },
      {
        Header: "Used",
        accessor: "Used",
        Filter: SliderColumnFilter,
        filter: filterGreaterThan,
        _isSorted: true,
      },
      {
        Header: "Quantity",
        accessor: "Quantity",
        Filter: NumberRangeColumnFilter,
        filter: "between",
        _isSorted: true,
      },
      {
        Header: "Created at",
        accessor: "created_at",
        // Filter: DateRangeColumnFilter,
        sortMethod: (a: any, b: any) => customSorting,
        _isSorted: true,
      },
    ],
  },
];
