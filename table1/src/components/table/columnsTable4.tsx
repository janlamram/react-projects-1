export const columnsTable4 = [
  {
    Header: "Table 4 - Simple table",
    isGlobalFilter: false,
    columns: [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Repo name",
        accessor: "repo.name",
      },
      {
        Header: "Type",
        accessor: "type",
        filter: "includes",
      },
      {
        Header: "Used",
        accessor: "Used",
      },
      {
        Header: "Quantity",
        accessor: "Quantity",
        filter: "between",
      },
      {
        Header: "Created at",
        accessor: "created_at",
      },
    ],
  },
];
