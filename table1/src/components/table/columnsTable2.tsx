import DefaultColumnFilter from "../../filters/defaultColumnFilter";
import SelectColumnFilter from "../../filters/selectColumnFilter";


export const columnsTable2 = [
  {
    Header: "Table 2 - Filtering and sorting",
    isGlobalFilter: true,
    columns: [
      {
        Header: "ID",
        accessor: "id",
        _isSorted: true,
      },
      {
        Header: "Repo name",
        accessor: "repo.name",
        Filter: DefaultColumnFilter,
        _isSorted: true,
      },
      {
        Header: "Type",
        accessor: "type",
        Filter: SelectColumnFilter,
        filter: "includes",
        _isSorted: true,
      },
      {
        Header: "Created at",
        accessor: "created_at",
        Filter: DefaultColumnFilter,
        _isSorted: true,
      },
    ],
  },
];
