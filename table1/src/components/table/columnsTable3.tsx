export const columnsTable3 = [
  {
    Header: "Table 3 - Sorting",
    isGlobalFilter: true,
    columns: [
      {
        Header: "ID",
        accessor: "id",
        _isSorted: true,
      },
      {
        Header: "Repo name",
        accessor: "repo.name",
        _isSorted: true,
      },
      {
        Header: "Type",
        accessor: "type",
        filter: "includes",
        _isSorted: true,
      },
      {
        Header: "Used",
        accessor: "Used",
        _isSorted: true,
      },
      {
        Header: "Quantity",
        accessor: "Quantity",
        filter: "between",
        _isSorted: true,
      },
      {
        Header: "Created at",
        accessor: "created_at",
        _isSorted: true,
      },
    ],
  },
];
