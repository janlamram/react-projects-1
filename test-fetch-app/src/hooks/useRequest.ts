import { useEffect, useState } from "react";

type State = {
  _data: Array<Record<string, string | number | boolean>> | undefined;
  isLoading: boolean;
};

export const useRequest = (request: () => Promise<any>): State => {
  const [state, setState] = useState({_data: undefined, isLoading: true});

  useEffect(() => {

    setTimeout(function(){ 
      request()
      .then((res) => setState({ _data: res, isLoading: false }))
      .catch(() => console.error("No data"));

     }, 4000);

    // request()
    //   .then((res) => setState({ _data: res, isLoading: false }))
    //   .catch(() => console.error("No data"));
  }, [request]);

  return state;
};
