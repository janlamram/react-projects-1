import React, { ReactElement, useEffect } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  useParams,
  useNavigate,
} from "react-router-dom";
import { Home } from "./views/home";
import { About } from "./views/about";


function Child() {
  // We can use the `useParams` hook here to access
  // the dynamic pieces of the URL.
  let { id } = useParams();
  const navigate = useNavigate();

  // let searchParams = new URLSearchParams(paramsString);

  useEffect(()=>{
  console.log('His ', navigate);
  },[])

  return (
    <div>
      <h3>ID: {id}</h3>
    </div>
  );
}


function App() {
  // const [data, setData] = useState({});

  // const getData = async (url: string) => {
  //   return await fetch(url).then((resp) => resp.json());
  // };

  // useEffect(() => {
  //   getData("json/data.json").then((data) => {
  //     console.log("Data +++ ", data);
  //     setData(data);
  //   });
  // }, []);

  return (
    <div>

      <Router>
        <div>
          <Link to="/home">Home</Link>
        </div>
        <div>
          <Link to="/about">About</Link>
        </div>
        <Routes>
          <Route path="/home" element={<Home />}></Route>
          <Route path="/about" element={<About />}></Route>
          <Route path="/home/:id" element={<Child />} />
          {/* <Route path="/:id" children={<Child />} /> */}
        </Routes>
      </Router>
    </div>
  );
}

export default App;
