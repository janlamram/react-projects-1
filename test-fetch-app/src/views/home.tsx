import { ReactElement } from "react";
import { Link } from "react-router-dom";
import { fetchData } from "../API/requests";
import { useRequest } from "../hooks/useRequest";

export const Home = (): ReactElement => {

  const { _data, isLoading } = useRequest(fetchData);


  return (
    <div>
      <div>HOME</div>
      {isLoading && <div>Loadddd</div>}
      <ul>
        {_data &&
          Object.entries(_data).map(
            ([key, val]: Array<any>, index: number) => {
              // console.log("Elele +++ key ", key);
              console.log("Elele +++ val ", val);
              return (
                <li key={index} style={{ listStyleType: "none" }}>
                  {index}
                  <Link to={`/home/${val.node_id}`}>
                    {" " + val.name}
                  </Link>
                </li>
              );
            }
          )}
      </ul>
    </div>
  );
};