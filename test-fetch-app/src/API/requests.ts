import getData from "./api";

export const fetchData = () => getData("json/data.json");
