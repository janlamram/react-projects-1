export const getData = async (url: string):  Promise<Record<string, number | boolean| string>> => {
  return await fetch(url).then((resp) => resp.json());
};

// export const getData = (url: string): Promise<Record<number, any> | any> => {
//   const resultPromise = new Promise((resolve, reject) => {
//       resolve(fetch(url).then((resp) => resp.json()));
//   })
//   return  resultPromise
// }

export default getData;